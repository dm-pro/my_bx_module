<?php
$MESS["MY_BX_MODULE_TAB_SETTINGS"] = "Настройки";
$MESS["MY_BX_MODULE_TAB_TITLE_SETTINGS_2"] = "Основные настройки модуля";
$MESS["MY_BX_MODULE_STRING_PARAM_TITLE"] = "Строковый демо-параметр";
$MESS["MY_BX_MODULE_CHECKBOX_PARAM_TITLE"] = "Чекбокс демо-параметр";
$MESS["MY_BX_MODULE_SAVE_BUTTON"] = "Сохранить";
$MESS["MY_BX_MODULE_RESET_BUTTON"] = "Отменить";