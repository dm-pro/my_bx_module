<?php

Class mybxmodule extends CModule
{
    var $MODULE_ID = "mybxmodule";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function mybxmodule()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = "Мой битрикс-модуль";
        $this->MODULE_DESCRIPTION = "Тестовый модуль для тренировки";
    }

    function InstallFiles()
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/mybxmodule/admin/mybxmodule.php", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/mybxmodule.php");
        return true;
    }

    function UnInstallFiles()
    {
        unlink($_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/mybxmodule.php");
        return true;
    }

    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->InstallFiles();
        RegisterModule("mybxmodule");
        $APPLICATION->IncludeAdminFile("Установка модуля mybxmodule", $DOCUMENT_ROOT."/local/modules/mybxmodule/install/step.php");
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->UnInstallFiles();
        UnRegisterModule("mybxmodule");
        $APPLICATION->IncludeAdminFile("Деинсталляция модуля mybxmodule", $DOCUMENT_ROOT."/local/modules/mybxmodule/install/unstep.php");
    }
}
?>