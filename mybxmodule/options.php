<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main;

/**
 * @global \CMain $APPLICATION
 * @global \CUser $USER
 * @global string $mid
 */

if (!$USER->IsAdmin()) return;
if (!Main\Loader::includeModule('mybxmodule')) return;

Loc::loadMessages(__FILE__);

$errorMessage = '';

$aTabs = array(
	array(
		"DIV" => "edit1",
		"TAB" => Loc::getMessage("MY_BX_MODULE_TAB_SETTINGS"),
		"ICON" => "",
		"TITLE" => Loc::getMessage("MY_BX_MODULE_TAB_TITLE_SETTINGS_2"),
	),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if($_POST['Update'] <> '' && check_bitrix_sessid())
{
	Main\Config\Option::set("mybxmodule", "string_param", $_POST['STRING_PARAM']);
	Main\Config\Option::set("mybxmodule", "checkbox_param", isset($_POST['CHECKBOX_PARAM']));
}
?>

<form method="post" action="<?= $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($mid)?>&lang=<?= LANG?>">
	<?php echo bitrix_sessid_post()?>
	<?php
	$tabControl->Begin();
	$tabControl->BeginNextTab();
	if ($errorMessage):?>
	<tr>
		<td colspan="2" align="center"><b style="color:red"><?=$errorMessage?></b></td>
	</tr>
	<?endif;?>
	<tr>
		<td width="40%"><?=Loc::getMessage("MY_BX_MODULE_STRING_PARAM_TITLE")?>:</td>
		<td width="60%"><input type="text" name="STRING_PARAM" value="<?= htmlspecialcharsbx(Main\Config\Option::get("mybxmodule", "string_param")) ?>" /></td>
	</tr>
	<tr>
		<td width="40%"><?=Loc::getMessage("MY_BX_MODULE_CHECKBOX_PARAM_TITLE")?>:</td>
		<td width="60%"><input type="checkbox" name="CHECKBOX_PARAM" value="Y" <?=((int)Main\Config\Option::get("mybxmodule", "checkbox_param")? 'checked':'')?> /></td>
	</tr>
	<?$tabControl->Buttons();?>
	<input type="submit" name="Update" class="adm-btn-save" value="<?= Loc::getMessage('MY_BX_MODULE_SAVE_BUTTON')?>">
	<input type="reset" name="reset" value="<?= Loc::getMessage('MY_BX_MODULE_RESET_BUTTON')?>">
	<?$tabControl->End();?>
</form>
