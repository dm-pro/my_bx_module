<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

global $USER;
if(!$USER->IsAdmin()){
  return;
}

$arMenu = array(
    "parent_menu" => "global_menu_settings",
    "sort" => 2000,
    "text" => Loc::getMessage("MY_BX_MODULE_MENU_TEXT"),
    "title" => Loc::getMessage("MY_BX_MODULE_MENU_TITLE"),
    "url" => "mybxmodule.php?lang=".LANGUAGE_ID,
    "icon" => "highloadblock_menu_icon",
    "items_id" => "mybxmodule_menu",
    "items" => array(
       array(
         "sort" => 10,
         "text" => Loc::getMessage("MY_BX_MODULE_MENU_ITEM_TEXT"),
         "url" => "mybxmodule.php?lang=".LANGUAGE_ID,
         "more_url" => array(),
         "title" => Loc::getMessage("MY_BX_MODULE_MENU_ITEM_TEXT"),
       ),
    ),
);

return $arMenu;